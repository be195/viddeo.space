(function() {

	let headBtn = document.querySelector('.vs-head-btn');
	let dialog = document.querySelector('dialog');
	let uploadArea = document.querySelector('#uploadArea');
	let uploadAreaLink = document.querySelector('#uploadAreaLink');
	let siofuId = document.querySelector('#siofu');
	let snackbar = document.querySelector('#toast');
	let progress = document.querySelector('#pl');
	let video = document.querySelector('.video');

	let loading = false;
	let videoT = {
		['3g2']: true,
		['3gp']: true,
		aaf: true,
		asf: true,
		avchd: true,
		avi: true,
		drc: true,
		flv: true,
		m2v: true,
		m4p: true,
		m4v: true,
		mkv: true,
		mng: true,
		mov: true,
		mp2: true,
		mp4: true,
		mpe: true,
		mpeg: true,
		mpg: true,
		mpv: true,
		mxf: true,
		nsv: true,
		ogg: true,
		ogv: true,
		qt: true,
		rm: true,
		rmvb: true,
		roq: true,
		svi: true,
		vob: true,
		webm: true,
		wmv: true,
		yuv: true
	};

	if(dialog) {
		let socket = io.connect();
		let uploader = new SocketIOFileUpload(socket);

		if(!dialog.showModal) {
			dialogPolyfill.registerDialog(dialog);
		}

		headBtn.addEventListener('click', function() {
			dialog.showModal();
		});

		dialog.querySelector('.close').addEventListener('click', function() {
			dialog.close();
		});

		uploadArea.addEventListener('dragover', function(e) {
			e.preventDefault();
			e.stopPropagation();	
		});

		function showHide(b) {
			if(b) {
				removeClass(uploadArea, 'hidden');
				removeClass(dialog.querySelector('.close'), 'hidden');
				progress.className += ' hidden';
			} else {
				uploadArea.className += ' hidden';
				dialog.querySelector('.close').className += ' hidden';
				removeClass(progress, 'hidden');
			}
		}

		function trialAndError(file) {
			if(file && file.name.split('.').pop() in videoT) {
				uploader.submitFiles([ file ]);
				showHide();
				loading = true;
			} else {
				snackbar.MaterialSnackbar.showSnackbar({ message: 'Error: not a video.' });
			}
		}

		socket.on('video', function(resp) {
			loading = false;

			if(!resp.success) {
				snackbar.MaterialSnackbar.showSnackbar({ message: 'Error: ' + resp.msg.substring(0, 48) + '...' });
				return showHide(true);
			}

			window.location.href = window.location.origin + '/show/' + resp.data;
		});

		socket.on('video_buff', function(data) {
			progress.MaterialProgress.setProgress(data.progress);
			progress.MaterialProgress.setBuffer(data.progress);
		});

		function removeClass(elements, myClass) {
			if (!elements) { return; }

			if (typeof(elements) === 'string') {
				elements = document.querySelectorAll(elements);
			}

			else if (elements.tagName) { elements=[elements]; }

			var reg = new RegExp('(^| )'+myClass+'($| )','g');

			for (var i=0; i<elements.length; i++) {
				elements[i].className = elements[i].className.replace(reg,' ');
			}
		}

		uploader.addEventListener('progress', function(e) {
			let percent = e.bytesLoaded / e.file.size;
			progress.MaterialProgress.setProgress(percent * 100);
			progress.MaterialProgress.setBuffer(100);
		});

		uploader.addEventListener('error', function(e) {
			snackbar.MaterialSnackbar.showSnackbar({ message: 'Error: ' + e.message.substring(0, 48) + '...' });
			return showHide(true);
		});

		uploadArea.addEventListener('drop', function(e) {
			e.preventDefault();
			e.stopPropagation();

			let file = e.dataTransfer.files[0];
			trialAndError(file);
		});

		uploadAreaLink.addEventListener('click', function() {
			siofuId.click();
		});

		siofuId.onchange = function(e) {
			let file = siofuId.files[0];
			trialAndError(file);	
		};

		window.onbeforeunload = function() {
			if (loading)
				return 'Video is processing, are you sure you want to quit?';
		};
	
		return;
	}

	video.addEventListener('click', function(e) {
		e.preventDefault();

		if(video.paused)
			video.play();
		else
			video.pause();
	});

})();