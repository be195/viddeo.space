const Sequelize = require('sequelize');

class Models {
	constructor(app) {
		this.app = app;
		this.sequelize = new Sequelize({
			dialect: 'sqlite',
			storage: 'db.db',
			pool: {
				max: 5,
				min: 0,
				acquire: 30000,
				idle: 10000
			}
		});

		this.sequelize.authenticate().then(() => {
			console.log('Connection has been established successfully.');
		}).catch(err => {
			console.error('Unable to connect to the database:', err);
		});

		this.models = {};
		this.loadModels();
	}

	loadModels() {
		this.models.File = this.sequelize.define('File', {
			hash: {
				type: Sequelize.STRING,
				allowNull: false
			},
			uuid: {
				type: Sequelize.STRING,
				allowNull: false
			},
			ip: {
				type: Sequelize.STRING,
				allowNull: false
			}
		}, {});

		this.sequelize.sync();
	}
}

module.exports = Models;
