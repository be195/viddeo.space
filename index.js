const app = {};
app.conf = require('./config');
app.uploads = [];

let web = require('./modules/web');
let crypt = require('./modules/crypt');
let models = require('./modules/models');

app.web = new web(app);
app.crypt = new crypt(app);
app.models = new models(app);

/////////////////////////////////

let fs = require('fs');

setInterval(function() {
	app.uploads.forEach(function(v, k) {
		if(Date.now() - v[1] > 86400000) {
			app.uploads[k] = undefined;
			fs.unlinkSync('videos/' + v[0] + '.webm');
		}
	});
}, 1000);

for(let file of fs.readdirSync('videos')) {
	fs.unlinkSync('videos/' + file);
}
